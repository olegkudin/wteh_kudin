var fs = require('fs');
var http = require('http');
var multiparty = require('multiparty');

let handler = (req, res) => {

    if(req.url === '/' && req.method === 'GET'){
        fs.readFile('./index.html', (err, data) => {
            if(err){
                res.write('Error loading html file');
                res.end();
                return;
            }
            res.write(data);
            res.end();
        });
        
        return;
    }

    if(req.url === '/students' && req.method === 'GET'){
        fs.readFile('./students/studentList.html', (err, data) => {
            if(err){
                res.write('Error loading html file');
                res.end();
                return;
            }
            res.write(data);
            res.end();
        });
        
        return;
    }

    if(req.url === '/students/add' && req.method === 'GET'){
        fs.readFile('./students/add/studentAdd.html', (err, data) => {
            if(err){
                res.write('Error loading html file');
                res.end();
                return;
            }
            res.write(data);
            res.end();
        });
        
        return;
    }

    if(req.url === '/students/add' && req.method === 'POST'){
        let form = new multiparty.Form();
        form.parse(req, (err, fields, files) => {
            if(err){
                res.write('Error parsing form');
                res.end();
                return;
            }
            
            fs.appendFile('./students/studentList.html', fields['studentFIO']+'\n', err => {
                if(err){
                    res.write('Error creating file');
                    res.end();
                    return;
                }
    
                res.writeHead(302, {
                    Location: '/'
                });
                res.end();
            });
        })
        return;
    }

    res.write('HTTP 404: Not Found');
	res.end();
}

http.createServer(handler).listen(3000);