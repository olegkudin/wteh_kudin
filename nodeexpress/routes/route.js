var controllers = require('../controllers');
var express = require('express');
var router = express.Router();


router.get('/', controllers.studentList);
router.all('/add', controllers.addStudent);

module.exports.router = router;
