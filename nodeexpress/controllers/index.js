var {indexController} = require('./index-controller');
var {studentList} = require('./student-list');
var {addStudent} = require('./add-student');

module.exports.indexController = indexController;
module.exports.studentList= studentList;
module.exports.addStudent= addStudent;

