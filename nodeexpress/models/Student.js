var fs = require('fs');

exports.Student = class Student{
    constructor(name, surname, age, studid, group)
    {
        this.name = name;
        this.surname =surname;
        this.age = age;
        this.studid = studid;
        this.group = group;
    }

    static getInfo(){
        return new Promise(resolve => {
            fs.readFile('models/Students.json', (err, data) => {
                let students = JSON.parse(data);
                resolve(students);
            });
        });
    }

    static addStudent(Student){
        return new Promise(resolve => {
            this.getInfo()
            .then(data => {
                data.push(Student);
                let res = JSON.stringify(data);
                fs.writeFile('models/Students.json', res, (err) => {
                    resolve({});
                });
            })
        });
    }
}