var http=require('http');
var bootstrap=require('./bootstrap/bootstrap');
var controllers=require('./controllers');
var {coolMiddleware}=require('./middlewares/collMiddleware');

bootstrap.registerRoute('/','GET',controllers.indexController);
bootstrap.registerRoute('/student','GET',controllers.StudentListController);
bootstrap.registerRoute('/student/add','GET',controllers.StudentFormController);
bootstrap.registerRoute('/student','POST',controllers.addStudentController);

bootstrap.useMiddleware(coolMiddleware);

bootstrap.setNotFound(controllers.notFoundController);

http.createServer(bootstrap.bootstrap).listen('8080');