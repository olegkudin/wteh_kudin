var fs=require('fs');

const dir='./views/';
const viewExtension='.html';

let view =(req,res,view,data)=>{
    fs.readFile(dir+view+viewExtension,(err, viewData)=>{
        if(err){
            res.write(err.code);
            res.end();
            return;
        }

        if(data){
            var tmpl=viewData.toString();
            var html=tmpl.replace('%',data.join('</li><li>'));
            res.write(html);
            res.end();
            return;
        }

        res.write(viewData);
        res.end();
    });
}

exports.view=view;