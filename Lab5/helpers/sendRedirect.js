let sendRedirect=(res,url)=>{
    res.writeHead(302,{
        Location: url
    });
    res.end();
}

exports.sendRedirect=sendRedirect;