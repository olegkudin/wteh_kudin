var {view} =require('../view-engine/view-engine');
var {Student}=require('../models/Student');

let StudentListController=(req,res)=>{
    Student.getAll()
        .then(students=>{
            view(req,res, 'StudentList',students);
        });
}

exports.StudentListController=StudentListController;