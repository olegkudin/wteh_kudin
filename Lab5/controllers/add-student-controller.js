var {Student}=require('../models/Student');
qs=require('querystring');
var {sendRedirect} =require('../helpers/sendRedirect');

let addStudentController=(req,res)=>{
    var body='';
    req.on('data',function(data){
        body+=data;
    });
    req.on('end',function(){

        var post=qs.parse(body);
        Student.addStudent({
                name:post['name'],
                age: post['age'],
                student_id: post['student_id']
            });
            sendRedirect(res,'/');
    });
}

exports.addStudentController=addStudentController;