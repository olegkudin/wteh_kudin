var Person =require("./Person");
class Student extends Person{
    constructor(name,age,student_id){
        super(name,age);
        this.student_id=student_id;
    }
    toString(){
        return `${this.name} ${this.age} ${this.student_id}`;
    }
    static addStudent(student){
        StudentList.push(new Student(student.name,student.age,student.student_id));
    }
    static getAll(){
        return new Promise((resolve,reject)=>{
            resolve(StudentList);
        })
    }
}
const StudentList=[];
StudentList.push(new Student('Stepan', 20,'31rqwr2'));


exports.Student=Student;