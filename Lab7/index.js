var express=require('express');
var hbs=require('express-handlebars');
var {router}=require('./routes');
var bodyParser=require('body-parser');

var app=express();
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(router);

app.engine('handlebars', hbs({defaultLayout: 'main'}));
app.set('view engine','handlebars');

app.listen(3000);