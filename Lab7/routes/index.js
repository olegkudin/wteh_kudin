var express=require('express');
var router=express.Router();
var controllers=require('../controllers');

router.get('/',controllers.indexController);
router.get('/technologies',controllers.technologiesController);
router.get('/technologies/add',controllers.addTechnologyController);
router.post('/technologies',controllers.handleTechnologyController);

module.exports.router=router;