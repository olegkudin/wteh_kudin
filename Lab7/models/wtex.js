'use strict';
module.exports = (sequelize, DataTypes) => {
  const wtex = sequelize.define('wtexes', {
    name: DataTypes.STRING,
    hours: DataTypes.INTEGER,
    teacherName: DataTypes.STRING
  }, {});
  wtex.associate = function(models) {
    // associations can be defined here
  };
  return wtex;
};