'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('wtexes', [{
        name: 'PHP',
        hours:20,
        teacherName:'Misha',
        createdAt:new Date(),
        updatedAt:new Date()
      },
      {
        name: 'JAVA',
        hours:15,
        teacherName:'Mark',
        createdAt:new Date(),
        updatedAt:new Date()
      }], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('wtexes', null, {});
  }
};
