var {Wtex}=require('../models');

module.exports.technologiesController=(req,res)=>{
    Wtex.findAll()
    .then(technologies=>{
        res.render('technologies',{technologies:technologies});
    });
}